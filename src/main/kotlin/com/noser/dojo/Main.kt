package com.noser.dojo

import tornadofx.launch

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        launch<MyApp>(args)
    }
}

