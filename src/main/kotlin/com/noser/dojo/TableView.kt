package com.noser.dojo

import javafx.collections.FXCollections.observableArrayList
import tornadofx.SmartResize
import tornadofx.View
import tornadofx.column
import tornadofx.tableview
import java.time.LocalDate
import java.time.LocalDate.of

class TableView : View() {
    private val persons = observableArrayList(
            Person(1, "Samantha Stuart", of(1981,12,4)),
            Person(2, "Tom Marks", of(2001,1,23)),
            Person(3, "Stuart Gills", of(1989,5,23)),
            Person(3, "Nicole Williams", of(1998,8,11))
    )

    override val root = tableview(persons) {
        column("ID", Person::id)
        column("Name", Person::name)
        column("Birthday", Person::birthday)
        columnResizePolicy = SmartResize.POLICY
    }
}

data class Person(val id: Int,val name: String,val birthday: LocalDate?)
